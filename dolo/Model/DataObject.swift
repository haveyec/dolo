//
//  DataObject.swift
//  dolo
//
//  Created by Havic on 3/15/18.
//  Copyright © 2018 Havic. All rights reserved.
//

import Foundation

struct DataObject {
    var name:String
    var location:String
    var distance:String
    
    init(dataDictionary:NSDictionary) {
        self.name = dataDictionary["name"] as! String
        let location = dataDictionary["location"] as! NSDictionary
        let cityLocation = location["city"] as? String
        self.location = cityLocation ?? "parts unknown"
        self.distance = String(describing: location["distance"]!)
    }
}
