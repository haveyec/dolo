//
//  SearchViewController+Extensions.swift
//  dolo
//
//  Created by Havic on 3/16/18.
//  Copyright © 2018 Havic. All rights reserved.
//

import Foundation
import CoreLocation

extension SearchViewController{
    
    func loadThatJson(lat:CLLocationDegrees, long:CLLocationDegrees) {
        let client_id = "3NAKZHVCMSBRZEBHSFXHNP4J5OBSCI4ZLQXYQFMJPWFNJOJQ"
        let client_secret = "WX2DBOSUOA2PI2QNA52VCNQ13LWAU1ZDIKW41FSSNA1MXQ3K"
        
        let url = "https://api.foursquare.com/v2/venues/search?ll=\(lat),\(long)&v=20180318&intent=checkin&limit=20&radius=50&client_id=\(client_id)&client_secret=\(client_secret)"
        
        let session = URLSession.shared
        let datatask: URLSessionTask = session.dataTask(with: URL(string:url)!) { (data, response, error) in
            let jsonDic:NSDictionary = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)) as! NSDictionary
            
            let dataDict = jsonDic["response"] as! NSDictionary
            let venuesArr = dataDict["venues"] as! [NSDictionary]
            
            for venue in venuesArr{
                let dataObjects = DataObject(dataDictionary: venue)
                self.dataObArr.append(dataObjects)
                self.filtered.append(dataObjects.name)
            }
            
            //Jump back to the main Queue and reload the collection to show data
            DispatchQueue.main.async {
                self.reloadJson?.reloadTableInfo()
            }
        }
        
        datatask.resume()
    }
}
