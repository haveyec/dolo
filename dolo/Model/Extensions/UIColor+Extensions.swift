//
//  UIColor+Extensions.swift
//  dolo
//
//  Created by Havic on 3/16/18.
//  Copyright © 2018 Havic. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    
    static func twlBlue()->UIColor{return UIColor(red: 56/255, green: 212/255, blue: 198/255, alpha: 1.0)}
    
    static func doloGradient(color1:UIColor, color2:UIColor)->[CGColor]{return Array(arrayLiteral: color1.cgColor,color2.cgColor)}
    
}
