//
//  ViewController.swift
//  dolo
//
//  Created by Havic on 3/14/18.
//  Copyright © 2018 Havic. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var logoMarkImage: UIImageView!
   let userDefaults = UserDefaults.standard
    
    
    override func viewWillAppear(_ animated: Bool) {
        if userDefaults.bool(forKey: "ispostCompleted") {
            logoMarkImage.image = UIImage(named: "btnCheck")
            
        }
        else {
            logoMarkImage.image = UIImage(named: "logomark")

        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let gradient = CAGradientLayer()
        let color1 = UIColor(red: 218/255, green: 101/255, blue: 141/255, alpha: 1)
        let color2 = UIColor(red: 122/255, green: 20/255, blue: 108/255, alpha: 1)
        gradient.frame = view.bounds
        gradient.colors = UIColor.doloGradient(color1: color1, color2: color2)
        view.layer.insertSublayer(gradient, at: 0)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        userDefaults.set(false, forKey: "ispostCompleted")
    }


}

