//
//  NewPostViewController.swift
//  dolo
//
//  Created by Havic on 3/14/18.
//  Copyright © 2018 Havic. All rights reserved.
//

import UIKit

class NewPostViewController: UIViewController,UITextViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var locationTextLbl: UILabel!
    @IBOutlet weak var headlineTxt: UITextField!
    let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var postBTN: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    var postCompleted:UIImageView!
    var ispostCompleted = false
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var myTextView: UITextView!
    
   
    override func viewWillAppear(_ animated: Bool) {
        
        guard let locationTitle = defaults.value(forKey: "locationTitle") as? String else { return //Set locationTextLbl not visibile by default unless location is found
            locationTextLbl.isHidden = true
        }
        
            locationTextLbl.isHidden = false
        locationTextLbl.text = locationTitle
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imagePicker.delegate = self
        let gradient = CAGradientLayer()
        let color1 = UIColor(red: 218/255, green: 101/255, blue: 141/255, alpha: 1)
        let color2 = UIColor(red: 122/255, green: 20/255, blue: 108/255, alpha: 1)
        gradient.frame = view.bounds
        gradient.colors = UIColor.doloGradient(color1: color1, color2: color2)
        view.layer.insertSublayer(gradient, at: 0)
        
        //Since UITextView does not have default place holder text going to work with UITextViewDelegate when setting up the view
        setUpTextView()
        
       
        
        //Set postBTN.isEnabled = false on initial load.
        postBTN.isEnabled = false
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Placeholder"
            textView.textColor = UIColor.lightGray
        }
        
        let numberOfLines = textView.numberOfLines()
        
        if numberOfLines > 3 {
            textView.isScrollEnabled = true
        }else{
            textView.isScrollEnabled = false
        }
        
    }
    
    func setUpTextView() {
        myTextView.delegate = self
        myTextView.text = "Placeholder"
        myTextView.textColor = .lightGray
        myTextView.textContainer.maximumNumberOfLines = 10
        headlineTxt.textColor = .black
        
        //Set a target to detect change in the headline text box
        headlineTxt.addTarget(self, action: #selector(changeInHeadlineTxt), for: .editingChanged)
    }
    
    @IBAction func newPostImagePicker(_ sender: UIButton) {
        imagePicker.allowsEditing = false
        imagePickerOptions()

    }
    
    @IBAction func cancelPostBTN(_ sender: UIButton) {
        ispostCompleted = false
        defaults.set(ispostCompleted, forKey: "ispostCompleted")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func newPostBTN(_ sender: UIButton) {
        ispostCompleted = true
        defaults.set(ispostCompleted, forKey: "ispostCompleted")
        
        //To avoid a bunch of modals all over the place just remove them all before we dismiss current view.
        self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.contentMode = .scaleAspectFit
            imageView.image = pickedImage
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    
    @objc func changeInHeadlineTxt() {
        if headlineTxt.text!.count > 0 {
            postBTN.isEnabled = true
        }else{
            postBTN.isEnabled = false
        }
        
    }
    
    func imagePickerOptions(){
        let alert = UIAlertController(title: "Add Image", message: "Which Media source you want to choose?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Saved Albums", style: .default, handler: { (action) in
            self.imagePicker.sourceType = .savedPhotosAlbum
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        
        self.present(alert, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}

//MARK: - UITextView
extension UITextView{
    
    func numberOfLines() -> Int{
        if let fontUnwrapped = self.font{
            return Int(self.contentSize.height / fontUnwrapped.lineHeight)
        }
        return 0
    }

}
