//
//  SearchViewController.swift
//  dolo
//
//  Created by Havic on 3/14/18.
//  Copyright © 2018 Havic. All rights reserved.
//

import UIKit
import CoreLocation

//Custom web services class to download the jSon information, to store in the table.
protocol ReloadJsonData{
    func reloadTableInfo()
}


class SearchViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,ReloadJsonData,UISearchBarDelegate {
    
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var searchResultsTbl: UITableView!
    let cellIdentifier = "cell"
    var currentLocation:CLLocationCoordinate2D!
    let managager = CLLocationManager()
    var dataObArr = [DataObject]()
    var reloadJson: ReloadJsonData?
    let searchController = UISearchController(searchResultsController: nil)
    var filtered = [String]()
    var myFilteredResults = [String]()
    var isSearching = false
    let defaults = UserDefaults.standard


    @IBOutlet weak var mySearchBar: UISearchBar!
    
    @IBOutlet weak var myTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        managager.delegate = self
        managager.requestWhenInUseAuthorization()
        managager.startUpdatingLocation()
        mySearchBar.delegate = self
        mySearchBar.returnKeyType = .done
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            print(location.coordinate)
            let ulatitude = location.coordinate.latitude
            let ulongitude = location.coordinate.longitude
            self.reloadJson = self
            loadThatJson(lat: ulatitude, long: ulongitude)
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching {
            return myFilteredResults.count
        }
        
        return dataObArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let venue = dataObArr[indexPath.row]
       
        if isSearching {
            cell.textLabel?.text = myFilteredResults[indexPath.row]
            cell.detailTextLabel?.text = venue.location + "    " + venue.distance + " miles"
        }
        
        else {
            cell.textLabel?.text = venue.name
            cell.detailTextLabel?.text = venue.location + "    " + venue.distance + " miles"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.textLabel?.text = myFilteredResults[indexPath.row]
        let currentCellText = cell.textLabel?.text
        defaults.setValue(currentCellText, forKey: "locationTitle")
    
        }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func cancelBTN(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func reloadTableInfo() {
        myTableView.reloadData()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false
            view.endEditing(true)
            reloadTableInfo()
        }
        
        else {
            isSearching = true
            
            myFilteredResults = filtered.filter({$0.contains(searchBar.text!)})
            
            reloadTableInfo()
        }
    }
    
}
